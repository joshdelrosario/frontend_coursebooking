//add an event to the register form

let registerForm = document.querySelector("#registerUser");
console.log(registerForm)

//add an event listener submit    pag cnlick si submit button kukunin lahat si input fields
registerForm.addEventListener('submit', (e) => {
	e.preventDefault()
	//prevents page redirection to avoid loss of input data whenever the registration process is not successful

//add query selector in all input fields
	let firstName= document.querySelector("#firstName").value; //.value -> to get the value of the inputs
	let lastName = document.querySelector("#lastName").value;
	let mobileNo = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2= document.querySelector("#password2").value;

//validation to enable submit button when all fields are populated and botch password match. And mobile no is equal to 11

	if((password1 !== '' && password2 !== '' ) && (password2 === password1) 
		&& (mobileNo.length === 11)) {

		//check for duplicate email in database first
		fetch('http://localhost:3000/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
			})
		.then(res => res.json())
		.then(data => {
			console.log(data);

		//if no duplicates found
		if(data === false) {
			fetch('http://localhost:3000/api/users/', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password1,
					mobileNo: mobileNo
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data === true) {
					//registration successful
					alert("registered successfuly")
					//redirect to login
					window.location.replace("./login.html");
				}else {
					alert("something went wrong");
				}
			})
			.catch((error) => {
				console.error('Error:', error);
			});
		}
		
		})
	}else {
		alert('Please check the information provided')
	}

})


